/*
* Instruções para compilar g++ -o out2 rembrandt.cpp -lX11 -lGL -lpthread -lpng -lstdc++fs
*/

#define OLC_PGE_APPLICATION
#include "olcPixelGameEngine.h"
#include<chrono>
using namespace std;

#define NTHREADS 4
#define DELAY 10
#define SIZE 500

olc::Pixel fb[SIZE][SIZE];

typedef struct {
	int start, end;
} t_Args;

class Example : public olc::PixelGameEngine
{
public:
	Example()
	{
		sAppName = "Rembrandt";
	}

public:
	bool OnUserCreate() override
	{
		// Called once at the start, so create things here
		return true;
	}

	bool OnUserUpdate(float fElapsedTime) override
	{
		// called once per frame
		for (int x = 0; x < ScreenWidth(); x++)
		{
			for (int y = 0; y < ScreenHeight(); y++)
			{
				Draw(x, y, fb[x][y]);
					
			}
		}
		return true;
	}
};

// void *drawPixels(void *arg){

// 	int r, g, b;
// 		for (int i = 0; i < SIZE; i++){
// 			for (int j = 0; j < SIZE; j++){
// 			r = (int)sqrt(pow(j - 0   , 2) + pow(i - 0   , 2));
// 			g = (int)sqrt(pow(j - 0   , 2) + pow(i - SIZE, 2));
// 			b = (int)sqrt(pow(j - SIZE, 2) + pow(i - SIZE, 2));

// 			fb[j][i] = olc::Pixel(r,g,b);
// 		}

// 		std::this_thread::sleep_for(std::chrono::milliseconds(10));
// 	}

// 	return 0;
// }


//Fractal function
void *drawPixels(void *arg){

	t_Args *args = (t_Args *) arg;
	std::cout << args->start << " " << args->end << endl;

	int r, g, b;
	for (int i = (args->start); i < (args->end); i++){
		for (int j = 0; j < SIZE; j++){
			
			r = (int)sqrt(pow(i - 0   , 2) + pow(j - 0   , 2));
			g = (int)sqrt(pow(i - 0   , 2) + pow(j - SIZE, 2));
			b = (int)sqrt(pow(i - SIZE, 2) + pow(j - SIZE, 2));

			fb[i][j] = olc::Pixel(r,g,b);
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(DELAY));
	}

	free(arg);
    pthread_exit(NULL);
	return 0;
}

void *startApp(void *arg){
	
	Example demo;
	if (demo.Construct(SIZE, SIZE, 1, 1)){
		demo.Start();
	}

	free(arg);
    pthread_exit(NULL);
	return 0;
}


int main() {
	//olc::Pixel pixeis[SIZE][SIZE];
	pthread_t tid_sistema[NTHREADS+1];
	t_Args *arg;
	int t;

	//Startins a thread to run the olcPixelEngine
	//thread do programa que é usado para desenhar na tela
	if (pthread_create(&tid_sistema[NTHREADS], NULL, startApp, (void*) arg)) {
			printf("--ERRO: pthread_create()\n"); 
			return -1;
		}

	//Thread for drawing the rembrandt fractal
	//Thread de desenho do fractal de rembrandt
	int step = SIZE/NTHREADS;
	for(t=0; t < NTHREADS;t++){
		
		arg = (t_Args *) malloc(sizeof(t_Args));
		if (arg == NULL) {
			printf("--ERRO: malloc()\n"); 
			return -1;
		}

		arg -> start = t*step;
		arg -> end = (t+1)*step;

		if (arg == NULL) {
			printf("--ERRO: malloc()\n"); 
			return -1;
		}

		if (pthread_create(&tid_sistema[t], NULL, drawPixels, (void*) arg)) {
			printf("--ERRO: pthread_create()\n"); 
			return -1;
		}	
	}
	
	for (t=0; t<NTHREADS; t++) {
		if (pthread_join(tid_sistema[t], NULL)) {
			printf("--ERRO: pthread_join() \n"); 
			return -1; 
		} 
	}
	
	getchar();
	return 0;
}
