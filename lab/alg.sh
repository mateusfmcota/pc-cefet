#!/bin/bash
range=15
numero=200
echo $range
echo "1 thread $numero numeros:"
for ((i=0; i <=$range; i++))
do
    ./out 1 $numero
done

echo ""
echo "2 thread $numero numeros:"
for ((i=0; i <=$range; i++))
do
   ./out 2 $numero
done

echo ""
echo "4 thread $numero numeros:"
for ((i=0; i <=$range; i++))
do
   ./out 4 $numero
done

echo ""
echo "6 thread $numero numeros:"
for ((i=0; i <=$range; i++))
do
   ./out 6 $numero
done

echo ""
echo "8 thread $numero numeros:"
for ((i=0; i <=$range; i++))
do
   ./out 8 $numero
done

echo ""
echo "16 thread $numero numeros:"
for ((i=0; i <=$range; i++))
do
   ./out 16 $numero
done
