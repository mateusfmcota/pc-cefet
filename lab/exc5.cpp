#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <chrono>
#include <cstdlib>

//#define NTHREADS  4
//#define TAM 220
//cria a estrutura de dados para armazenar os argumentos da thread
typedef struct {
	// int idThread;
	int inicio;
	int fim;
	int *vet;
} t_Args;

//funcao executada pelas threads
void *PrintHello (void *arg) {
	t_Args *args = (t_Args *) arg;
//	auto x = args->vet;
	printf("%d %d \n",args->inicio, args->fim);

	//std::cout<< args->tam << std::endl;
	free(arg);

	pthread_exit(NULL);
	return 0;
}

void *incrementa1(void *arg){
	t_Args *args = (t_Args *) arg;

	for(int i = args->inicio; i<args->fim; i++){
		*(args->vet + i) += 1;
	}

	free(arg);

	pthread_exit(NULL);
	return 0;

}

void imprimeVetor (int *vetor, int tamanho) {
	for (int i = 0; i < tamanho; i++){
		printf("%d ", vetor[i] );
	}
	printf("\n\n");
}

//funcao principal do programa
int main(int argc, char* argv[]) {

	int NTHREADS = atoi(argv[1]);
	int TAM = atoi(argv[2]);
	// printf("%d %d %d \n", argc ,NTHREADS, TAM);

	using namespace std::chrono;
	pthread_t tid_sistema[NTHREADS];
	int t;
	t_Args *arg; //receberá os argumentos para a thread

	// Le e escreve numeros nele
	// printf("Digite o tamanho do vetor: ");
	// int tam = 0;
	// scanf("%d",&tam);

	//std::cin >> tam;
	int tam = TAM;

	// Cria o vetor de acordo com o exemplo
	int vet[tam];
	for(int i = 0; i<tam; vet[i++] = i);
	// imprimeVetor(vet, tam);

	//Calcula a quantidade de passos de acordo com a quantidade de threads
	auto start = high_resolution_clock::now();
	int step = tam/NTHREADS;
	for(t=0; t<NTHREADS; t++) {
		// printf("--Aloca e preenche argumentos para thread %d\n", t+1);
		arg = (t_Args *) malloc(sizeof(t_Args));
		if (arg == NULL) {
			printf("--ERRO: malloc()\n");
			return -1;
		}
		//Aqui calcula-se a quantidade de numeros no vetor que serão calculados por cada thread, como o seu inincio e fim
		int step = tam/NTHREADS;
		// arg->idThread = t+1;
		// Isso faz com que cada thread receba metade(é um codigo ruim, mas que funciona para o caso)
		// O step que é calculado antes serve para calcular o inicio e o fim
		arg->vet = &vet[0];
		arg->inicio = t*step;
		arg->fim = (t+1)*step;
		// printf("--Cria a thread %d\n", t+1);
		if (pthread_create(&tid_sistema[t], NULL, incrementa1, (void*) arg)) {
			printf("--ERRO: pthread_create()\n");
			return -1;
		}
	}

	//--espera todas as threads terminarem
	for (t=0; t<NTHREADS; t++) {
		if (pthread_join(tid_sistema[t], NULL)) {
			printf("--ERRO: pthread_join() \n");
			return -1;
		}
	}

	auto end = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>(end - start);

	// imprimeVetor(vet, tam);

	printf("%d ms \n",duration);

	// printf("--Thread principal terminou\n");
	//getchar();
	pthread_exit(NULL);
}
